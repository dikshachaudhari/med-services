import $ from 'jquery';
import "lazysizes";
import "../styles/styles.css";
import MobileMenu from "./modules/MobileMenu";  //to maintain the readability MobileMenu naam same rakha hai
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";

// Handles Mobile menu/Header
let mobileMenu = new MobileMenu();

// Added smooth scroll functionality to our header links
new SmoothScroll();

// Added active link status in our project
new ActiveLinks();

// Added a modal for form
new Modal();

//Handles Reveal on scroll
new RevealOnScroll($("#our-beginning"));
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));

if(module.hot){
    module.hot.accept();
}
//alert("Hello World From Med Services");
// console.log("This goes to console");
// console.log("Another Line!!");