const currentTask = process.env.npm_lifecycle_event; // gives the name of the current task i.e dev or build
// whenever we hit npm run dev or build it generates a process in that process it has environment
const path = require('path');

const {CleanWebpackPlugin} = require('clean-webpack-plugin');  //it is used for cleaning perspective

const MiniCssExtractPlugin = require('mini-css-extract-plugin'); 

const HtmlWebpackPlugin = require('html-webpack-plugin'); // it exports a class HtmlWebpackPlugin
const fse = require('fs-extra');

const postCSSPlugins = [
    require('postcss-import'),
    require('postcss-mixins'),
    require('postcss-simple-vars'),
    require('postcss-nested'),
    require('autoprefixer')
]

// Custom PLUGIN
class TaskAfterCompilation {
    apply(compiler){
        compiler.hooks.done.tap('Copying images ... ' , function(){
           fse.copySync('./app/assets/images', './dist/assets/images'); 
        });
    }
}

let cssConfig = {
    test: /\.css/i, // in js regex is written between '/regex-comes-here/'
    use: [
//        'style-loader', // style tag mai daalta hai css ko
        'css-loader?url=false', //css ko load karta hai

        {
            loader : 'postcss-loader',
            options : {
                postcssOptions: {
                    plugins : postCSSPlugins
                }
            }
        }
        // 'url = false is written to tell the loader that i will handle the background : url('') manually'
    ]
}
let config = {
    entry: './app/assets/scripts/app.js',
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html', // yeh bolta hai ki file ka naam kya rakhna hai
            template: './app/index.html' // yeh batata hai ki file kaha se uthaana hai
        })
    ],
    //watch : true,
    module: {
        rules: [
            cssConfig
        ],
    }
    
};
// hot module replacement
// hot iskeliye kyuki vo runtime pe inject karega ....
if(currentTask == 'dev'){
    config.output = {
        filename: 'app.bundled.js',
        path: path.resolve(__dirname, 'app') // this method will give me the curr dir + app
    };
    
    config.devServer = {
        before: function(app,server){
            server._watch('./app/**/*.html')
        },
        contentBase: path.resolve(__dirname, 'app'),
        port: 3000,
        hot: true ,// 'hot module' feature allows injecting css and js without refreshing
        // it automatically keeps watch so no need to write watch : true
        host: '0.0.0.0', //mere ghar k andar jitne bhi devices hai sab isko access kar sakte hai
        // open : true
    };
    
    config.mode = 'development';// the default mode is this only but it is good to specify!!
    
    cssConfig.use.unshift('style-loader');
}

if(currentTask == 'build'){
    config.mode = 'production';
    
    config.output = {
        filename: '[name].[chunkhash].bundled.js',
        path: path.resolve(__dirname, 'dist')
    };
    config.optimization = {
        splitChunks: {
            chunks: 'all',
        }
    };
    
    config.plugins.push (
        new CleanWebpackPlugin(), // it will empty the dist folder first and add the files
        new MiniCssExtractPlugin({
           filename: 'styles.[chunkhash].css'  // chunkhash is a hashcode related to the file  
        }),
        new TaskAfterCompilation()
    );
    
    cssConfig.use.unshift(MiniCssExtractPlugin.loader); // MiniCssExtractPlugin has a static loader

    postCSSPlugins.push(require('cssnano'));
}
module.exports = config;